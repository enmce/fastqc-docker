FROM ubuntu:14.04

MAINTAINER "Viktor Svekolkin" <enmce@yandex.ru>

RUN apt-get update && apt-get install -y \
	wget \
	perl \
	unzip \
	openjdk-6-jre

#Download FastQC
RUN wget -P /opt http://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.11.3.zip

WORKDIR /opt
#Unzip FastQC and remove archive after that
RUN unzip fastqc_v0.11.3.zip && \
	rm /opt/fastqc_v0.11.3.zip

WORKDIR /opt/FastQC
#Set entrypoint on fastqc perl script
ENTRYPOINT ["perl", "fastqc"]




	
