#FastQC docker image

This repository contains Dockerfile that packages FastQC 0.11.3 Docker image.

FastQC 0.11.3 can be downloaded [here](http://www.bioinformatics.babraham.ac.uk/projects/download.html#fastqc) and documentation can be found [here](http://www.bioinformatics.babraham.ac.uk/projects/fastqc/).


### The recommended way to build an image is:

```
docker build -t fastqc:0.11.3 .
```

### To run FastQC 0.11.3 in container:
Let`s say we want to produce quality report for our reads files.
If read file <name>.fq is in ~/path_to_reads/, following command will produce output report files <name>_fastqc.html and <name>_fastqc.zip in directory ~/path_to_reads. 
```
docker run -v ~/path_to_reads:/mnt -u $(echo $UID) --rm fastqc:0.11.3 /mnt/<name>.fq
```
